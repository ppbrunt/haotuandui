//
//  PuchViewController.m
//  ZKPuch
//
//  Created by pp on 16/4/18.
//  Copyright © 2016年 pp. All rights reserved.
//http://m.haotuandui.com/api/UtdPunch

#import "PuchViewController.h"
#import <AFNetworking.h>
//#import "ViewController.h"

//@class ViewController;
@interface PuchViewController ()
@property (nonatomic,strong) NSString *punchType;
@property (nonatomic,strong) NSMutableDictionary *parameter;
@property (weak, nonatomic) IBOutlet UITextField *phoneName;
@property (weak, nonatomic) IBOutlet UITextField *adress;
@property (weak, nonatomic) IBOutlet UITextField *uuid;
@property (weak, nonatomic) IBOutlet UITextField *latitude;
@property (weak, nonatomic) IBOutlet UITextField *longitude;
@property (weak, nonatomic) IBOutlet UITextField *macAdderss;
@property (weak, nonatomic) IBOutlet UITextField *wifiName;
@property (weak, nonatomic) IBOutlet UITextField *data;



@end

@implementation PuchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    /// 获取HTTP头部Cookie
    NSHTTPCookieStorage * cookiejar=[NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSLog(@"%@",cookiejar);
    NSUserDefaults  *defaults   = [NSUserDefaults standardUserDefaults];
    /*
     * 把cookie进行归档并转换为NSData类型
     * 注意：cookie不能直接转换为NSData类型，否则会引起崩溃。
     * 所以先进行归档处理，再转换为Data
     */
    NSData *cookiesData =[NSKeyedArchiver archivedDataWithRootObject:[cookiejar cookies]];
    //直接保存Cookie
    [defaults setValue:cookiesData forKey:@"allCookie"];
//    取出token
    NSArray *cookies =[NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"allCookie"]];
    if (cookies){
        NSHTTPCookieStorage *cookieStorage= [NSHTTPCookieStorage sharedHTTPCookieStorage];
        for (id cookie in cookies ){
            NSLog(@"比较前-AppDelegate这打印：--->%@",[(NSHTTPCookie *)cookie valueForKey:@"value"]);
            [cookieStorage setCookie:(NSHTTPCookie *)cookie]; //把保存在本地的Cookie 设置进去
            NSLog(@"比较后-AppDelegate这打印：--->%@",[(NSHTTPCookie *)cookie valueForKey:@"value"]);
        }
    }

    self.sessionId = [[NSUserDefaults standardUserDefaults]objectForKey:@"SessionId"];
    NSString *uuid = [[NSUUID UUID] UUIDString];
    NSLog(@"%@",uuid);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)goToWork:(UIButton *)sender {
    self.punchType = @"1";
    [self updata];
    
    
}
- (IBAction)afferWork:(id)sender {
    
    self.punchType = @"2";
    [self updata];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES]; //实现该方法是需要注意view需要是继承UIControl而来的
}

- (void)updata
{
    [self setParameters];
    NSString *url =@"http://m.haotuandui.com/api/UtdPunch";
    AFHTTPSessionManager *manger  = [AFHTTPSessionManager manager];
    [manger.requestSerializer setValue:@"m.haotuandui.com" forHTTPHeaderField:@"Host"];
    [manger.requestSerializer setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [manger.requestSerializer setValue:self.sessionId forHTTPHeaderField:@"SessionId"];
    [manger.requestSerializer setValue:@"close" forHTTPHeaderField:@"Connection"];
//    [manger.requestSerializer setValue:[NSString stringWithFormat: @"SessionId=%@",self.sessionId] forHTTPHeaderField:@"Cookie"];
    [manger.requestSerializer setValue:@"Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.3" forHTTPHeaderField:@"User-Agent"];
    [manger.requestSerializer setValue:@"314" forHTTPHeaderField:@"Content-Length"];
    [manger.requestSerializer setValue:@"3.0.7" forHTTPHeaderField:@"HTDAppVersion"];
    [manger.requestSerializer setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
    
    
    [manger POST:url parameters:self.parameter progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        UIAlertView *ale = [[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@"%@",responseObject] delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil , nil];
        [ale show];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error );
        UIAlertView *ale2 = [[UIAlertView alloc]initWithTitle:@"错误" message:[NSString stringWithFormat:@"%@",error] delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil , nil];
        [ale2 show];

//        [self presentViewController:self.loginVC animated:YES completion:nil];
    }];
    
}

- (void)setParameters{
    
//    DeviceName	iPhone5s(iPhone 5S)
//    Latitude	22.282672
//    PunchType	2
//    Longitude	113.534210
//    DeviceId	afa7aaedf799a3ce936595a80197f1955b2f11ed
//    MacAddress	6c:59:40:63:e2:36
//    WifiName	ppppppppp
//    PlaceName	广东省珠海市香洲区梅华西路737号
//    AttendanceDate	2016-04-22
//    ShiftId	16762
    [self p:_phoneName.text no:@"iPhone5s(iPhone 5S)" key:@"DeviceName"];
    [self p:_latitude.text no:@"22.224987" key:@"Latitude"];
    [self.parameter setObject:self.punchType forKey:@"PunchType"];
    [self p:_longitude.text no:@"113.493733" key:@"Longitude"];
    [self p:_uuid.text no:@"afa7aaedf799a3ce936595a80197f1955b2f11ed" key:@"DeviceId"];
    [self p:_macAdderss.text no:@"6c:59:40:63:e2:36" key:@"MacAddress"];
    [self p:_wifiName.text no:@"interlube" key:@"WifiName"];
    [self p:_adress.text no:@"广东省珠海市香洲区虹达路3号" key:@"PlaceName"];
    [self.parameter setObject:self.shiftId forKey:@"ShiftId"];
    NSString *date;
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    date = [formatter stringFromDate:[NSDate date]];
    
    [self p:_data.text no:date key:@"AttendanceDate"];
    
    
}
- (IBAction)goBackLogin:(UIButton *)sender {
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"SessionId"];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)p:(NSString *)p no:(NSString *)no key :(NSString *)key
{
    if (!p||[p isEqualToString:@""]) {
        [self.parameter setObject:no forKey:key];

    }else{
        [self.parameter setObject:p forKey:key];
    }
}


- (NSMutableDictionary *)parameter
{
    if (!_parameter) {
        _parameter = [[NSMutableDictionary alloc]init];
    }
    return _parameter;
}

@end
