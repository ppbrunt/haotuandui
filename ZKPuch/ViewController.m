//
//  ViewController.m
//  ZKPuch
//
//  Created by pp on 16/4/18.
//  Copyright © 2016年 pp. All rights reserved.
// http://pt.im2x.com/api/Login4Other?pwd=2524586&accountId=15919133321&softId=1035


#import "ViewController.h"
#import "PuchViewController.h"

@interface ViewController ()
@property (nonatomic,strong) NSDictionary *parameter;
@property (nonatomic,strong) NSArray *companyValue;
@property (strong,nonatomic) PuchViewController *puck;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSString *ud =  [[[UIDevice currentDevice] identifierForVendor]UUIDString];
    ud =[ud lowercaseString];
    NSLog(@"%@",ud);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    for (UIButton *btm in self.compayBtms) {
        [btm setHidden:YES];
    }
}
- (void)viewDidAppear:(BOOL)animated
{
    self.tokenId = [[NSUserDefaults standardUserDefaults]objectForKey:@"SessionId"];
    [super viewDidAppear:animated];
    if (self.tokenId) {
        [self presentViewController:self.puck animated:YES completion:nil];[[NSUserDefaults standardUserDefaults]objectForKey:@"SessionId"];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES]; //实现该方法是需要注意view需要是继承UIControl而来的
}- (IBAction)sure:(UIButton *)sender {
    
    [self.activity startAnimating];
    NSString *url = @"http:pt.im2x.com/api/Login4Other";http://pt.im2x.com/api/Login4Other?pwd=2524586&accountId=15919133321&softId=1035
    self.parameter =@{@"accountId" : self.account.text,
                      @"pwd" : self.password.text,
                      @"softId" : self.softId.text};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:url parameters:self.parameter progress:^(NSProgress * _Nonnull downloadProgress) {
        NSLog(@"downloadProgress>>>>%@",downloadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        NSDictionary *dic = responseObject;
        self.tokenId = [dic valueForKey:@"TokenId"];
        NSArray *companys = [dic valueForKey:@"MyEnts"];
        self.companyValue = companys;
        for (int i=0; i<self.companyValue.count;i++ ) {
            NSDictionary *dic = self.companyValue[i];
            [self.compayBtms[i] setHidden:NO];
            [((UIButton *)self.compayBtms[i]) setTitle:dic[name] forState:UIControlStateNormal];
            
        }
        [self.activity stopAnimating];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        UIAlertView *ale =[ [UIAlertView alloc]initWithTitle:@"登陆错误" message: [NSString stringWithFormat:@"%@",error] delegate:nil cancelButtonTitle:@"好吧" otherButtonTitles:nil, nil];
        [ale show];
        [self.activity stopAnimating];
    }];
    
}
- (IBAction)chooseCompay:(UIButton *)sender {
    [self.activity startAnimating];
    
    //    http://pt.im2x.com/api/Login4Other?tokenId=5cacefe0-1e43-4214-90fc-36ab0eaa66df&entId=52870&softId=1035
    //    http://m.haotuandui.com/api/Login?tokenId=bccecef9-1514-4727-8bbd-e02da124b55e
    NSDictionary *pra = @{@"tokenId": self.tokenId,
                          @"entId":self.companyValue[sender.tag][@"EntId"],
                          @"softId" : self.softId.text};
    //    [[NSUserDefaults standardUserDefaults] setObject:self.softId forKey:@"EntId"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer =[AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //    [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [manager GET:@"http://pt.im2x.com/api/Login4Other" parameters:pra progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSData *dataString = responseObject;
        NSString *token = [[NSString alloc]initWithData:dataString encoding:NSUTF8StringEncoding];
        NSRange rang =NSMakeRange(1, token.length-2);
        token = [token substringWithRange:rang];
        [self getToken:token];
        NSLog(@"%@",token);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
        [self.activity stopAnimating];
    }];
    
    
}

- (void)getToken:(NSString *)string{
    NSLog(@"%@",string);
    NSDictionary *dic =@{@"tokenId" :string};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer =[AFHTTPRequestSerializer serializer];
    [manager GET:@"http://m.haotuandui.com/api/Login" parameters:dic progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        NSDictionary * resuit = responseObject;
        NSString *sessionId = resuit[@"SessionId"];
        [[NSUserDefaults standardUserDefaults]setObject:sessionId forKey:@"SessionId"];
        self.tokenId = sessionId;
        NSLog(@"%@",sessionId);
        [self getShiftId];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
        [self.activity stopAnimating];
    }];
    
    
}

- (void)getShiftId{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer =[AFHTTPRequestSerializer serializer];
    [manager GET:@"http://m.haotuandui.com/api/UtdMy?getMyShiftWorkHourForRemind=" parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        NSDictionary * resuit = responseObject;
        NSArray *arr = [resuit valueForKeyPath:@"UtdShiftWorkHourForRemindModelList.ShiftId"];
        NSString *string = arr[0];
        self.ShiftId = string;
        [[NSUserDefaults standardUserDefaults]setObject:self.ShiftId forKey:@"ShiftId"];
        NSLog(@"%@",string);
        [self presentViewController:self.puck animated:YES completion:nil];
        [self.activity stopAnimating];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
        [self.activity stopAnimating];
    }];
    

     }

- (NSDictionary *)parameter
{
    if (!_parameter) {
        _parameter = [[NSDictionary alloc]init];
    }
    return _parameter;
}


- (PuchViewController *)puck{
    if (!_puck) {
        UIStoryboard *main= [UIStoryboard storyboardWithName:@"Main" bundle:NULL];
        _puck = [main instantiateViewControllerWithIdentifier:@"puck"];
    }
    _puck.shiftId=[[NSUserDefaults standardUserDefaults]objectForKey:@"ShiftId"];
    return _puck;
}

@end

