//
//  ViewController.h
//  ZKPuch
//
//  Created by pp on 16/4/18.
//  Copyright © 2016年 pp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
//                {
//EntAdminName = "\U9ec3\U8fea\U5eb7";
//EntId = 36046;
//EntName = "\U8679\U8fbe\U8def3\U53f72\U680b";
//TotalCount = 56;
//},
//{
//    EntAdminName = "\U5f6d\U76fc";
//    EntId = 52870;
//    EntName = "\U7231\U5ba0\U5e73\U53f0";
//    TotalCount = 2;



const NSString *adminName = @"EntAdminName";
const NSString *enId = @"EntId";
const NSString *name = @"EntName";
const NSString *TotalCount = @"TotalCount";


@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *account;

@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *softId;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *compayBtms;
@property (strong ,nonatomic) NSString *tokenId;
@property (strong ,nonatomic) NSString *ShiftId;

@end
